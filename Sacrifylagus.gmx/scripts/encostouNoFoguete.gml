// encostouNoFoguete()

return
        instance_place(x, y, Bico) != noone
        || instance_place(x, y, Foguete) != noone
        || instance_place(x, y, Cauda) != noone
        || instance_place(x, y, Garra) != noone;

