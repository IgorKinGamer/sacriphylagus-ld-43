// atualizarLado(teclaNeg, teclaPos, atual)

var teclaNeg, teclaPos, atual;
teclaNeg = argument0;
teclaPos = argument1;
atual    = argument2;

if (keyboard_check_pressed(teclaNeg))
    return -1;
if (keyboard_check_pressed(teclaPos))
    return 1;
if (keyboard_check_released(teclaNeg))
    if (keyboard_check(teclaPos))
        return 1;
    else
        return 0;
if (keyboard_check_released(teclaPos))
    if (keyboard_check(teclaNeg))
        return -1;
    else
        return 0;

// Não aconteceu nada
return atual;
