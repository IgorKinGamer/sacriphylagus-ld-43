// mostrarPeso(índice (0, 1, ...), sprite, peso)

var ind  = argument0;

var spr  = Foguete.sprites[|ind];
var qnt  = Foguete.quantidades[|ind];
var peso = Foguete.pesos[|ind];

var yy = yBasePesos + (1+ind)*dyEntreItens;

// Redimensionamento
var redim = 32 / max(sprite_get_width(spr), sprite_get_height(spr));
draw_sprite_ext(
        spr, 0, xItens, yy,
        redim, redim, 0, c_white, 1
);

usarFonte(fnt_Pesos, fa_center, fa_middle);
draw_text(xQuants, yy, string(round(qnt)));
draw_text(xPesos, yy, string(round(peso)));

