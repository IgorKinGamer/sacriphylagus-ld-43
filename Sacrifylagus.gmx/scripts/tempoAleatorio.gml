// tempoAleatorio(min, max)
// Em segundos

var v1 = argument0;
var v2 = argument1;

return round(random_range(v1, v2) * room_speed);

