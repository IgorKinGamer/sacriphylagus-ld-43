// verificarColisaoComObstaculo(dano)

var dano = argument0;

var parte;

parte = instance_place(x, y, Garra);
if (parte == noone)
    parte = instance_place(x, y, Bico);
if (parte == noone)
    parte = instance_place(x, y, Cauda);
if (parte == noone)
    parte = instance_place(x, y, Foguete);

// Encostou em alguma parte
if (parte != noone)
{
    // Garra - destrói, tira da lista
    if (parte.object_index == Garra)
    {
        ControleJogo.pontos += 50;
        if (parte.direita)
            Foguete.nGarrasDir--;
        else
            Foguete.nGarrasEsq--;
        Foguete.quantidades[|Foguete.indGarras]--;
        with (parte)
            instance_destroy();
        var ind = ds_list_find_index(Foguete.garras, parte);
        ds_list_delete(Foguete.garras, ind);
    }
    else
    {
        // Afeta
        parte.vida -= dano;
        if (parte.vida < 0)
        {
            switch (parte.object_index)
            {
                case Foguete:
                    with (parte)
                        instance_destroy();
                    audio_play_sound(som_Perdeu, 0, false);
                    instance_create(0, 0, Reiniciador);
                    break;
                case Bico:
                    Foguete.quantidades[|Foguete.indBico] = 0;
                    with (parte)
                        instance_destroy();
                    Foguete.bico = noone;
                    break;
                case Cauda:
                    Foguete.quantidades[|Foguete.indCauda] = 0;
                    with (parte)
                        instance_destroy();
                    Foguete.cauda = noone;
                    break;
            }
        }
    }

    instance_destroy();
    
    audio_play_sound(som_Destruicao, 0, false);
}

