// usarFonte(fonte, alinH, alinV)

var fonte = argument[0];
var alinH = fa_left;
var alinV = fa_top;
var cor   = c_black;

if (argument_count >= 2)
    alinH = argument[1];
if (argument_count >= 3)
    alinV = argument[2];
if (argument_count >= 4)
    cor = argument[3];

draw_set_font(fonte);
draw_set_halign(alinH);
draw_set_valign(alinV);
draw_set_colour(cor);

